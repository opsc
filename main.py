#!/usr/bin/env python

"""
A possible use of odfpy for the `Openbaar Schrijver' book.
Written by Open Source Publishing.

http://ospublish.constantvzw.org/works/valentine-scripting
"""

try:
    import spices
    from stylesheet import text_style
    from liblove import directory_structure
except ImportError:
    raise

if __name__ == "__main__":
    directory_structure()
    for genre in ["brief", "verhaal", "gedicht"]:
        # import stylesheet is an easy way of getting at these arrays...
        for style in text_style[genre].keys():
            char_odts = spices.Character(genre, style)
            char_odts.loop_over_spices()
            ding_odts = spices.Dingbat(genre, style)
            ding_odts.loop_over_spices()
            img_odts = spices.ImageSpice(genre, style)
            img_odts.loop_over_spices()
